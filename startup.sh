#!/bin/bash
# run with 'source startup.sh' to set the environment variables
# then run 'vault status' in new terminal to check the connection

# Set the Vault address
export VAULT_ADDR='http://127.0.0.1:8200'

# Set the Vault token
read -p "Enter Token: " VAULT_TOKEN
export VAULT_TOKEN

echo "VAULT_ADDR and VAULT_TOKEN have been set."
